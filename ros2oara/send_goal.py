import threading
import rclpy
import rclpy.utilities
import rclpy.logging
import time
from rclpy.executors import MultiThreadedExecutor
from ros2node.api import NodeNameCompleter
from ros2topic.api import TopicTypeCompleter, TopicMessagePrototypeCompleter

from oara_interfaces.msg import GoalState
from oara.cli.send_goal import SendGoal
from oara.cli.cli import CLI

from .verb import VerbExtension


class SendGoalVerb(VerbExtension):
    """Send a goal to an OARA actor"""

    def add_arguments(self, parser, cli_name):
        super().add_arguments(parser, cli_name)

        arg = parser.add_argument('actor', type=str, help='Actor name')
        arg.completer = NodeNameCompleter()

        arg = parser.add_argument('task', help='Task type of the actor; e.g., "std_msgs/String"')
        arg.completer = TopicTypeCompleter()

        arg = parser.add_argument('goal', nargs='?', default='{}', help='Goal to send')
        arg.completer = TopicMessagePrototypeCompleter(topic_type_key='task')

        parser.add_argument('--sleep', type=float, default=1.0,
                            help="Sleep a given amount of seconds between each request")

    def main(self, *, args):
        rclpy.init()
        cli = CLI()
        cli.header()

        node = SendGoal('oara_send_goal')
        node.get_logger().set_level(rclpy.logging.LoggingSeverity.DEBUG)
        executor = MultiThreadedExecutor()
        t = threading.Thread(target=rclpy.spin, args=[node, executor])
        t.start()

        # Retry the connection until the discovery of other nodes happens.
        # Workaround for https://github.com/ros2/ros2/issues/1057
        retrials = 2
        retry_delay = 2.0
        connected = node.connect(args.actor)
        while not connected and retrials > 0:
            node.get_logger().warning(f"Cannot find topic oara/request in node {args.actor}. Retrying in {retry_delay} seconds. {retrials} retrials left.")
            retrials -= 1
            time.sleep(retry_delay)
            connected = node.connect(args.actor)

        if not connected:
            node.get_logger().warning(f"Cannot find topic oara/request in node {args.actor}")
            node.get_logger().error(f"Actor {args.actor} seems to be unconfigured!")
            return

        state = GoalState(state=GoalState.UNKNOWN)

        try:
            if not node.send(request=args.task, msg=args.goal):
                node.get_logger().error(f"Could not send goal to actor {args.actor}")

            else:
                # state = node.result()
                polling_rate = node.create_rate(1 / args.sleep)
                last_state = GoalState.UNKNOWN
                while True:
                    state = node.poll_state()

                    if state.state == GoalState.FINISHED:
                        node.get_logger().info(f"FINISHED {state}")
                        break
                    elif state.state == GoalState.DROPPED:
                        node.get_logger().warn(f"DROPPED {state}")
                        break
                    elif state.state == GoalState.FORMULATED and state.state < last_state:
                        node.get_logger().warn(f"FORMULATED {state}")
                        break
                    elif state.state in (GoalState.SELECTED, GoalState.EXPANDED,
                                         GoalState.COMMITTED) and state.state < last_state:
                        node.get_logger().info(
                            f"Goal in state {state.state} from state {last_state}. Traverse its lifecycle again")
                        node.retry_goal()
                    elif state.state == GoalState.DISPATCHED:
                        node.get_logger().debug(f"DISPATCHED {state}")
                    elif state.state == GoalState.EVALUATED:
                        node.get_logger().info(f"EVALUATED {state}")
                    elif state.state == GoalState.UNKNOWN:
                        node.get_logger().warn(f"UNKNOWN {state}")
                    else:
                        node.get_logger().error(f"Invalid state {state}")

                    last_state = state.state
                    polling_rate.sleep()

        except KeyboardInterrupt:
            node.drop()

        except Exception as ex:
            node.get_logger().error(f"{ex}")
            cli.console.print_exception(show_locals=True)

        finally:
            state = node.state
            if state.state > GoalState.UNKNOWN and state.state < GoalState.FINISHED:
                node.drop()

        rclpy.shutdown()
        node.destroy_node()
        t.join()
