from ros2cli.node.strategy import add_arguments
from ros2cli.node.strategy import NodeStrategy
from ros2node.api import NodeNameCompleter, get_node_names, get_absolute_node_name
from ros2param.api import call_get_parameters
from ..api import header, modules_info
from ..verb import VerbExtension                                      

class InfoVerb(VerbExtension):
    """Get information about a OARA module."""

    def add_arguments(self, parser, cli_name):  # noqa: D102
        add_arguments(parser)
        arg = parser.add_argument(
            'node_names', nargs='*', help='Name of the ROS node')
        parser.add_argument('--viewer', action="store_true", help="generate a JSON file for the viewer")
        parser.add_argument('--dot', action="store_true", help="generate a Dot file")
        parser.add_argument('--output', help="output file if needed")
        arg.completer = NodeNameCompleter()

    def main(self, *, args):  # noqa: D102
        with NodeStrategy(args) as node:
            architecture = modules_info(node, args.node_names)

            print(header)
            if args.viewer or args.dot:
                import pygraphviz
                graph = pygraphviz.AGraph(directed=True, strict=False, nodesep="1", ranksep="2")
                
                for actor in architecture["actors"]:
                    graph.add_node(actor["name"], 
                                   shape="rect", 
                                   width="3.46", 
                                   height="1.67")
                    for c in actor["children"]:
                        response = call_get_parameters(node=node, node_name=actor["node"],
                                                       parameter_names=[f"{c}.actor"])
                        act = response.values[0].string_value
                        graph.add_edge(actor['name'], act, 
                                       constraint=True)
                    for d in actor["data"]:
                        response = call_get_parameters(node=node, node_name=actor["node"],
                                                       parameter_names=[f"{d}.observer"])
                        obs = response.values[0].string_value
                        graph.add_edge(obs, actor['name'],
                                       label=d, style="dashed", constraint=False)
                    for e in actor["events"]:
                        response = call_get_parameters(node=node, node_name=actor["node"],
                                                       parameter_names=[f"{e}.observer"])
                        obs = response.values[0].string_value#.split('/')
                        graph.add_edge(obs, actor['name'], 
                                       label=e, constraint=False)

                for observer in architecture["observers"]:
                    graph.add_node(observer["name"], 
                                   shape="cylinder", 
                                   width="2.08", 
                                   height="2.77")

                graph.layout(prog='dot')

            else:
                for module in architecture['actors']:
                    print(f"--- Module {module['node']} ---")
                    print(f"- actor ({module['type']} pattern)")
                    if module['task']:
                        print(f"- accepts goals for task: {module['task']}\n  - service: {module['srv']}")
                    if module['data']:
                        print(f"- data required:")
                        for d in module['data']:
                            print(f"  - {d}")
                    if module['events']:
                        print(f"- events received:")
                        for d in module['events']:
                            print(f"  - {d}")
                    if module['children']:
                        print(f"- dispatches to:")
                        for d in module['children']:
                            print(f"  - {d}")
                    print()

                for module in architecture['observers']:
                    print(f"--- Module {module['node']} ---")
                    print(f"- observer ({module['type']} pattern)")
                    if module['data']:
                        print(f"- data provided:")
                        for d in module['data']:
                            print(f"  - {d}")
                    if module['events']:
                        print(f"- events emitted:")
                        for d in module['events']:
                            print(f"  - {d}")
                    print()


            if args.dot:
                if args.output:
                    graph.write(args.output)
                else:
                    graph.write('file.dot')
                print("Dot graph generated")

            if args.viewer:
                _, _, width, height = graph.graph_attr['bb'].split(',')
                architecture['graph'] = [float(width), float(height)]
                for a in architecture["actors"]:
                    n = graph.get_node(a["name"])
                    x, y = n.attr['pos'].split(',')
                    a["position"] = {"x": float(x), "y": float(y)}
                for a in architecture["observers"]:
                    n = graph.get_node(a["name"])
                    x, y = n.attr['pos'].split(',')
                    a["position"] = {"x": float(x), "y": float(y)}

                for e in graph.iteroutedges():
                    path = e.attr['pos'].split(' ')
                    try:
                        x, y = e.attr['lp'].split(',')
                    except:
                        x = 0
                        y = 0
                    architecture["arcs"].append({
                        "path": [[float(p.split(",")[0]), float(p.split(",")[1])] for p in path[1:]],
                        "label": e.attr['label'],
                        "data": e.attr['constraint'] == "False",
                        "position": [float(x), float(y)]
                    })

                if args.output:
                    with open(args.output, "w") as f:
                        import json
                        json.dump(architecture, f)
                    print(f"Architecture description for Viewer written to file {args.output}")

                else:
                    print(architecture)