from typing import List
from collections import defaultdict
from ros2node.api import get_node_names, get_absolute_node_name

from .api import get_module_information

def modules_info(node: 'rclpy.node.Node', required_modules: List[str]):
    modules = []
    architecture = defaultdict(list)

    node_names = get_node_names(node=node)

    if len(required_modules) == 0:
        for node_name in node_names:
            try:
                get_module_information(node=node, node_name=node_name.full_name)
            except:
                continue
            modules.append(node_name)

    else:
        modules_names = [get_absolute_node_name(n) for n in required_modules]
        for n in node_names:
            if n.full_name in modules_names:
                modules.append(n)

        unfound = set(modules_names) - set(n.full_name for n in modules)
        if unfound:
            raise KeyError(unfound)

    for module in modules:
        info = get_module_information(node=node, node_name=module.full_name)
        module_type = info.get('type', 'module').upper()
        if module_type == 'ACTOR':
            architecture["actors"].append({
                "name": module.name,
                "type": info.get('pattern', 'Actor').capitalize(),
                "task": info.get('task', ''),
                "srv": info.get('srv', ''),
                "lifecycle": info.get("pattern", ""),
                "node": module.full_name,
                "data": [x for x in info.get("data", [])],
                "events": [x for x in info.get("events", [])],
                "children": [x for x in info.get("children", [])]
            })
        elif module_type == 'OBSERVER':
            architecture["observers"].append({
                "name": module.name,
                "node": module.full_name,
                "type": info.get('pattern', 'Actor').capitalize(),
                "data": [x for x in info.get("data", [])],
                "events": [x for x in info.get("events", [])],
            })

    return architecture