header = '''
 .d88888b.        d8888 8888888b.         d8888 
d88P" "Y88b      d88888 888   Y88b       d88888 
888     888     d88P888 888    888      d88P888 
888     888    d88P 888 888   d88P     d88P 888 
888     888   d88P  888 8888888P"     d88P  888 
888     888  d88P   888 888 T88b     d88P   888 
Y88b. .d88P d8888888888 888  T88b   d8888888888 
 "Y88888P" d88P     888 888   T88b d88P     888 
'''                               
                                                

manifest_interface_template = """<?xml version="1.0"?>
<?xml-model href="http://download.ros.org/schema/package_format3.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
<package format="3">
  <name>{{ package.name }}</name>
  <version>{{ package.version }}</version>
  <description>{{ package.description }}</description>
  {% for person in package.maintainers %}
  <maintainer email="{{ person.email }}">{{ person.name }}</maintainer>
  {% endfor %}
  {% for person in package.authors %}
  <author email="{{ person.email }}">{{ person.name }}</author>
  {% endfor %}
  {% for l in package.licenses %}
  <license>{{ l }}</license>
  {% endfor %}

  <buildtool_depend>ament_cmake</buildtool_depend>

  {% for dep in package.build_depends %}
  <depend>{{ dep }}</depend>
  {% endfor %}

  {% for dep in package.test_depends %}
  <test_depend>{{ dep }}</test_depend>
  {% endfor %}

  {% for group in package.member_of_groups %}
  <member_of_group>{{ group }}</member_of_group>
  {% endfor %}

  <export>
    <build_type>ament_cmake</build_type>
  </export>
</package>
"""


cmake_interface_template = """cmake_minimum_required(VERSION 3.5)

project({{ package_name }})

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(oara_interfaces REQUIRED)
find_package(builtin_interfaces REQUIRED)
find_package(rosidl_default_generators REQUIRED)
{% for dep in dependencies %}
find_package({{ dep }} REQUIRED)
{% endfor %}

ament_index_get_resource(FORMULATE_TEMPLATE oara_template_formulate oara_interfaces)
ament_index_get_resource(GETTER_TEMPLATE oara_template_getter oara_interfaces)

#
# Here, you can create the Formulate interface for your OARA Actors
# 
# The macro below takes as parameters the name of the messages
# corresponding to Actor Tasks
#
# oara_generate_formulate_interface(
#  "geometry_msgs/Vector3"
#  "{{ package_name }}/MyTask"
#  )

#
# Here, you can create the Getter interface for your OARA Modules
# 
# The macro below takes as parameters the name of the messages
# corresponding to the data exchanged between Actors and Observers
#
# oara_generate_getter_interface(
#  "sensor_msgs/BatteryState"
#  "{{ package_name }}/MyData"
#  )

#
# This macro is mandatory to actually generate the ROS2 interface messages
# You must add your base messages (tasks, data) and additional interfaces
# you may need in your code.
#
rosidl_generate_interfaces(${PROJECT_NAME}
    # add your original messages here:
    # "msg/MyTask.msg"
    # "msg/MyData.msg"
    # generated OARA interface
    ${OARA_GENERATED_FORMULATE_INTERFACES}
    ${OARA_GENERATED_GETTER_INTERFACES}
    DEPENDENCIES
    {% for dep in dependencies %}
      {{ dep }}
    {% endfor %}
      builtin_interfaces 
      oara_interfaces
    ADD_LINTER_TESTS
)

# Export package dependencies
ament_export_dependencies(ament_cmake)
ament_export_dependencies(rosidl_default_runtime)
ament_export_dependencies(builtin_interfaces)
ament_export_dependencies(oara_interfaces)
{% for dep in dependencies %}
ament_export_dependencies({{ dep }})
{% endfor %}

ament_package()
"""
