from typing import List, Tuple, Dict

from sympy import im

from .api import get_module_information
from ros2lifecycle.api import call_get_states
from ros2node.api import get_node_names
from rclpy.node import Node

def list_modules(node: Node) -> Tuple[Dict[str, str], Dict[str, str]]:
    actors = {}
    observers = {}

    node_names = get_node_names(node=node)

    for node_name in node_names:
        try:
            info = get_module_information(node=node, node_name=node_name.full_name)
        except:
            continue

        if info is None: continue

        t = info.get('type', 'module')
        if t == 'actor':
            actors[node_name.full_name] = info
        elif t == 'observer':
            observers[node_name.full_name] = info

        states = call_get_states(node=node, node_names=list(actors.keys())+list(observers.keys()))
        for n, state in states.items():
            if isinstance(state, Exception): continue
            if n in actors: actors[n] = state.label
            if n in observers: observers[n] = state.label

    return actors, observers