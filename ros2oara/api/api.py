import json
import importlib
import rclpy

from rclpy.parameter import PARAMETER_SEPARATOR_STRING
from ros2node.api import get_node_names as get_all_node_names
from ros2param.api import call_get_parameters, call_list_parameters, get_value
                       
from ...internal import MsgType


def _is_actor(node_name, service_names_and_types):
    for (service_name, service_types) in service_names_and_types:
        if (
            service_name == f'{node_name}/oara/select' and
            'oara_interfaces/srv/Select' in service_types
        ):
            return True
    return False

def insert_dict(dictionary, key, value):
        split = key.split(PARAMETER_SEPARATOR_STRING, 1)
        if len(split) > 1:
            if not split[0] in dictionary:
                dictionary[split[0]] = {}
            insert_dict(dictionary[split[0]], split[1], value)
        else:
            dictionary[key] = value

def get_module_information(*, node, node_name):
    params = [p for p in call_list_parameters(node=node, node_name=node_name) if p.startswith('oara.model')]
    if len(params) == 0:
      raise KeyError
    response = call_get_parameters(node=node, node_name=node_name, parameter_names=params)
    info = {'oara': {'model': dict()}}
    for i in range(len(response.values)):
      insert_dict(info, key=params[i], value=get_value(parameter_value=response.values[i]))
    return info['oara']['model']

def sleep(node, delay):
  start_time = node.get_clock().now()
  now = start_time
  time_to_go = delay
  while rclpy.ok() and time_to_go > 0:
    rclpy.spin_once(node, timeout_sec=time_to_go)
    now = node.get_clock().now()
    elapsed = (now - start_time).nanoseconds / rclpy.utilities.S_TO_NS
    time_to_go = delay - elapsed 