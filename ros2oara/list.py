import rclpy
from oara.cli.list import List
from .verb import VerbExtension


class ListVerb(VerbExtension):
    """Output the list of OARA actors."""

    def add_arguments(self, parser, cli_name):
        super().add_arguments(parser, cli_name)
        parser.add_argument('--namespace', type=str,
            help='Namespace to inspect for the oara/module_info topic')

    def main(self, *, args):  # noqa: D102
        rclpy.init()
        node = List("ros2oara_list", namespace=args.namespace)
        try:
            rclpy.spin(node)
        except KeyboardInterrupt:
            rclpy.shutdown()