import sys

from ros2cli.node.strategy import add_arguments
from ros2cli.node.strategy import DirectNode, NodeStrategy
from ros2node.api import NodeNameCompleter, get_node_names, get_absolute_node_name
from ros2lifecycle.api import call_get_states, call_change_states

from lifecycle_msgs.msg import State, Transition

from ..api import header
from ..api.api import get_module_information
from ..verb import VerbExtension                                      

class ChangeStateVerb(VerbExtension):
    """Change lifecycle state of some OARA module."""
    transition_label = 'create'

    def add_arguments(self, parser, cli_name):  # noqa: D102
        add_arguments(parser)
        arg = parser.add_argument(
            'modules', nargs='*', help='Name of the OARA modules')
        arg.completer = NodeNameCompleter()
        parser.add_argument('-a', '--all', help=f'{self.transition_label} all OARA modules', action='store_true')

    def main(self, *, args):  # noqa: D102
        with NodeStrategy(args) as node:
            node_names = get_node_names(node=node)
            modules = []

            if args.all:
                for node_name in node_names:
                    try:
                        info = get_module_information(node=node, node_name=node_name.full_name)
                        if info:
                            modules.append(node_name)
                    except:
                        pass

            else:
                if len(args.modules) == 0:
                    return "You must either use the --all option, or give at least one module name."

                modules_names = [get_absolute_node_name(n) for n in args.modules]
                modules = []
                for n in node_names:
                    if n.full_name in modules_names:
                        print(f"found module {n.full_name}")
                        modules.append(n)

                unfound = set(modules_names) - set(n.full_name for n in modules)
                if unfound:
                    return f'Modules not found: {unfound}'

            print(header)
            states = dict()
            for n in modules:
                #print(f"get state of {n.full_name}")
                states.update(call_get_states(node=node, node_names=[n.full_name]))
            #print(states)

            for module, state in states.items():
                if isinstance(state, Exception): continue
                print(f'Module {module} is currently in state {state.label} [{state.id}]')
                print(f'changing state of {module}...')
                results = call_change_states(node=node, transitions={module: Transition(label=self.transition_label)})
                result = results[module]
                if isinstance(result, Exception):
                    print('Exception while calling service of node '
                        f"'{args.node_name}': {result}", file=sys.stderr)
                elif result:
                    print(f'Changed state of module {module}.')
                else:
                    print(f'Error changing state of module {module}', file=sys.stderr)
        return

class ConfigureVerb(ChangeStateVerb):
    """Configure OARA modules."""
    transition_label = 'configure'

class CleanupVerb(ChangeStateVerb):
    """Cleanup OARA modules."""
    transition_label = 'cleanup'

class ActivateVerb(ChangeStateVerb):
    """Activate OARA modules."""
    transition_label = 'activate'

class DeactivateVerb(ChangeStateVerb):
    """Deactivate OARA modules."""
    transition_label = 'deactivate'

class ShutdownVerb(ChangeStateVerb):
    """Shutdown OARA modules."""
    transition_label = 'shutdown'
