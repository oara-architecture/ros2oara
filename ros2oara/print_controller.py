import rclpy
from ros2topic.api import TopicTypeCompleter

from oara.actors.print_controller import PrintController
from oara.cli.msg_types import request_msg_from_name
from oara.internal.main import main as oara_main
from oara.cli.cli import CLI

from .verb import VerbExtension

class PrintVerb(VerbExtension):
    """ Print Controller"""

    def add_arguments(self, parser, cli_name):
        super().add_arguments(parser, cli_name)
        arg = parser.add_argument('task', type=str,
            help='Request message type defining the accepted task; e.g., "oara_std_interfaces/msg/StringGoal"')
        arg.completer = TopicTypeCompleter()

        parser.add_argument('-s', '--sleep', type=float, default=1.0,
            help="Sleep a given amount of seconds before finishing goal")

    def main(self, *, args):
        rclpy.init()
        cli = CLI()
        cli.header()
        try:
            msg_module = request_msg_from_name(args.task)
        except Exception:
            cli.console.print_exception(show_locals=True)
        oara_main(PrintController, msg_module, sleep=args.sleep)
