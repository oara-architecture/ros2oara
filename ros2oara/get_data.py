import threading
import rclpy
import rclpy.utilities
from ros2node.api import NodeNameCompleter

from oara.cli.get_data import GetData
from oara.cli.cli import CLI

from .verb import VerbExtension


class GetDataVerb(VerbExtension):
    """Get a data from an OARA observer"""

    def add_arguments(self, parser, cli_name):
        super().add_arguments(parser, cli_name)

        arg = parser.add_argument('observer', type=str, help='Observer node')
        arg.completer = NodeNameCompleter()

        arg = parser.add_argument('data', help='data name', type=str)

    def main(self, *, args):
        rclpy.init()
        cli = CLI()
        cli.header()

        node = GetData('oara_get_data')
        node.get_logger().set_level(rclpy.logging.LoggingSeverity.DEBUG)

        if not node.connect(args.observer, args.data):
            node.get_logger().warning(f"Cannot find data {args.data} in node {args.observer}")
            node.get_logger().error(f"Check node name and configuration status!")
            return

        t = threading.Thread(target=rclpy.spin, args=[node])
        t.start()

        header, value = node.get(args.data)
        cli.print_message(header)
        cli.print_message(value)

        rclpy.shutdown()
        t.join()