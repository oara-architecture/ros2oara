import getpass
import os
import shutil
import subprocess
import sys
import jinja2

from catkin_pkg.package import Dependency
from catkin_pkg.package import Export
from catkin_pkg.package import Package
from catkin_pkg.package import Person

from ros2pkg.api.create import create_package_environment, _create_folder

from ..verb import VerbExtension
from ..api import header, cmake_interface_template, manifest_interface_template

class CreateVerb(VerbExtension):
    """Create a new ROS2 OARA package."""

    def add_arguments(self, parser, cli_name):
        parser.add_argument(
            'package_name',
            help='The package name')
        parser.add_argument(
            '--description',
            default='TODO: Package description',
            help='The description given in the package.xml')
        parser.add_argument(
            '--license',
            default='TODO: License declaration',
            help='The license attached to this package')
        parser.add_argument(
            '--destination-directory',
            default=os.curdir,
            help='Directory where to create the package directory')
        parser.add_argument(
            '--dependencies',
            nargs='+',
            default=[],
            help='list of dependencies')
        parser.add_argument(
            '--maintainer-email',
            help='email address of the maintainer of this package'),
        parser.add_argument(
            '--maintainer-name', default=getpass.getuser(),
            help='name of the maintainer of this package'),

    def main(self, *, args):
        maintainer = Person(args.maintainer_name)

        if args.maintainer_email:
            maintainer.email = args.maintainer_email
        else:
            # try getting the email from the global git config
            git = shutil.which('git')
            if git is not None:
                p = subprocess.Popen(
                    [git, 'config', '--global', 'user.email'],
                    stdout=subprocess.PIPE)
                resp = p.communicate()
                email = resp[0].decode().rstrip()
                if email:
                    maintainer.email = email
            if not maintainer.email:
                maintainer.email = maintainer.name + '@todo.todo'

        build_type = 'ament_cmake'
        deps = ['oara_interfaces',
                'rosidl_default_generators',
                'builtin_interfaces']

        test_dependencies = []
        if build_type == 'ament_cmake':
            test_dependencies = ['ament_lint_auto', 'ament_lint_common']
        if build_type == 'ament_python':
            test_dependencies = ['ament_copyright', 'ament_flake8', 'ament_pep257',
                                 'python3-pytest']

        buildtool_depends = [build_type]
        package = Package(
            package_format=3,
            name=args.package_name,
            version='0.0.0',
            description=args.description,
            maintainers=[maintainer],
            licenses=[args.license],
            buildtool_depends=[Dependency(dep) for dep in buildtool_depends],
            build_depends=[Dependency(dep) for dep in (args.dependencies + deps)],
            test_depends=[Dependency(dep) for dep in test_dependencies],
            exports=[Export('build_type', content=build_type)],
            member_of_groups=['rosidl_interface_packages']
        )

        package_path = os.path.join(args.destination_directory, package.name)
        if os.path.exists(package_path):
            return '\nAborted!\nThe directory already exists: ' + package_path + '\nEither ' + \
                'remove the directory or choose a different destination directory or package name'

        print(header)

        print(f'going to create a new OARA Interface package')
        print('package name:', package.name)
        print('destination directory:', os.path.abspath(args.destination_directory))
        print('version:', package.version)
        print('description:', package.description)
        print('maintainer:', [str(maintainer) for maintainer in package.maintainers])
        print('licenses:', package.licenses)
        print('dependencies:', [str(dependency) for dependency in package.build_depends])

        package_directory = _create_folder(package.name, args.destination_directory)
        if not package_directory:
            return 'unable to create folder: ' + args.destination_directory

        template = jinja2.Template(cmake_interface_template)
        cmake = template.render(package_name=package.name, dependencies=args.dependencies)
        with open(os.path.join(package_directory, 'CMakeLists.txt'), 'w') as h:
            h.write(cmake)

        template = jinja2.Template(manifest_interface_template)
        cmake = template.render(package=package)
        with open(os.path.join(package_directory, 'package.xml'), 'w') as h:
            h.write(cmake)
