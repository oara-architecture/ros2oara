import os
from setuptools import setup, find_packages

setup(
    name='ros2oara',
    version='0.1.0',
    # Packages to export
    packages=find_packages(exclude=['resource']),
    # Files we want to install, specifically launch files
    data_files=[
        # Install marker file in the package index
        ('share/ament_index/resource_index/packages', ['resource/ros2oara']),
        # Include our package.xml file
        (os.path.join('share', 'ros2oara'), ['package.xml']),
    #    # Include all launch files.
    #    (os.path.join('share', package_name, 'launch'), glob('*.launch.py'))
    ],
    # This is important as well
    install_requires=['setuptools'],
    zip_safe=True,
    author='Charles Lesire',
    author_email='charles.lesire@onera.fr',
    maintainer='Charles Lesire',
    maintainer_email='charles.lesire@onera.fr',
    keywords=['OARA', 'ONERA', 'ROS2', 'Control Architecture'],
    description="OARA ROS2 CLI",
    license='ONERA',
    entry_points={
        'ros2cli.command': [
            'oara = ros2oara.oara:OaraCommand',
        ],
        'ros2cli.extension_point': [
            'ros2oara.verb = ros2oara.verb:VerbExtension',
        ],
        'ros2oara.verb': [
            'list = ros2oara.list:ListVerb',
            'print = ros2oara.print_controller:PrintVerb',
            'send_goal = ros2oara.send_goal:SendGoalVerb',
            'get_data = ros2oara.get_data:GetDataVerb',
        ],
    },
)